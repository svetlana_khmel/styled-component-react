import { combineReducers } from 'redux';
import displayComponent from './displayComponentReducer';

const rootReducer = combineReducers({
  displayComponent,
});


export default rootReducer;

