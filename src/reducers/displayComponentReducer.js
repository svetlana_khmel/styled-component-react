import { DISPLAY_COMPONENT } from '../common/actionTypes';

let initialState  = {};

export default (state = initialState, action) => {
  switch (action.type){
    case DISPLAY_COMPONENT:
      return {...state, data: action.payload }

    default:
      return state;
  }
}
