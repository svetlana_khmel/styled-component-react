import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as CustomComponents from 'react-cl';

class Display extends Component {
  // constructor (props) {
  //   super(props);
  //
  // }

  renderComponent = () => {
    const prop = this.props.component;
    const CusButton =  CustomComponents[this.props.component];

    if (!prop) return null;

    return  <CusButton />
  }

  render () {
    return(<div>
      { this.renderComponent() }
    </div>)
  }
}


const mapStateToProps = ({displayComponent}) => {
    return {
      component: displayComponent.data
    }
}

//export default Display;
export default connect(mapStateToProps, null)(Display);