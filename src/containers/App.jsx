import React, { Component } from 'react';
import SideBar from './SideBar';
import Display from './Display';
import Button from './Button/Button';
import InputButton from './Button/Input';

import Link from './Button/Link';

class App extends Component {
  render() {
    return (
      <div className="App">
         <h1>el=Input Button</h1>

         {/*<div>*/}
           {/*<Input styleTabel={'Default Button'} icon={'icon'} iconPosition={'left'} />*/}
         {/*</div>*/}

          {/*<div>
            <InputButton styleType={'primary'} label={'Primary Button'} />
          </div>*/}
          {/*<div>
            <Input styleType={'secondary'} label={'Secondary Button'} />
          </div>
          <div>
            <Input styleType={'tertiary'} label={'Tertiary Button'} />
          </div>
          <div>
            <Input styleType={'clearance'} label={'Clearance Button'} />
          </div>
          <div>
            <Input styleType={'homedelivery'} label={'Homedelivery Button'} />
          </div>
          <div>
            <Input styleType={'clickcollect'} label={'Clickcollect Button'} />
          </div>
          <div>
            <Input styleType={'disabled'} label={'Disabled Button'} />
          </div>*/}

        <h1>el=Button</h1>

        <div>
          <Button label={'Default Button'} />
        </div>

        <div>
          <Button label={'Default Button'} icon={'icon'} iconPosition={'left'} />
        </div>

        <div>
          <Button type={'primary'} label={'Primary Button'} />
        </div>
        <div>
          <Button type={'secondary'} label={'Secondary Button'} />
        </div>
        <div>
          <Button type={'tertiary'} label={'Tertiary Button'} />
        </div>
        <div>
          <Button type={'clearance'} label={'Clearance Button'} />
        </div>
        <div>
          <Button type={'homedelivery'} label={'Homedelivery Button'} />
        </div>
        <div>
          <Button type={'clickcollect'} label={'Clickcollect Button'} />
        </div>
        <div>
          <Button type={'disabled'} label={'Disabled Button'} />
        </div>

          <h1>el=Link</h1>
         <div>
           <Link label={'Default Link'} />
         </div>
          <div>
            <Link type={'primary'} label={'Primary Link'} />
          </div>
          <div>
            <Link type={'secondary'} label={'Secondary Link'} />
          </div>
          <div>
            <Link type={'tertiary'} label={'Tertiary Link'} />
          </div>
          <div>
            <Link type={'clearance'} label={'Clearance Link'} />
          </div>
          <div>
            <Link type={'homedelivery'} label={'Homedelivery Link'} />
          </div>
          <div>
            <Link type={'clickcollect'} label={'Clickcollect Link'} />
          </div>
          <div>
            <Link type={'disabled'} label={'Disabled Link'} />
          </div>
          <div>
            <Link type={'clickcollect'} label={'text Link'} />
          </div>

          <div>
            <Link type={'text'} label={'Text Link'} />
          </div>

        MULTI-LINE BUTTONS - 2 VERSIONS

        <h1>el=miltiline</h1>

        <div>
          <Button type={'multiline'} multiline={'span'} mainText={'Finaliser ma commande'} subText={' toute commande oblige à son paiement'} />
        </div>
        <div>
          <Button type={'multiline'} multiline={'small'}  mainText={'Finaliser ma commande'} subText={' toute commande oblige à son paiement'}  />
        </div>

        <h1>el=ajax</h1>
        <div>
          <Button type={'primary'} ajax={'true'} label={'Primary Button'} iconPosition={'left'} />
        </div>
        <div>
          <Button type={'secondary'} ajax={'true'} label={'Secondary Button'} iconPosition={'left'} />
        </div>

        <h1>el=disable</h1>
        <div>
          <Button type={'selected'} label={'Selected Button'} disabled />
        </div>

        <h1>el=Alternative</h1>
        <div>
          <Link type={'tertiary-alt'} label={'Tertiary-alt button'} />
        </div>
        <div>
          <Link type={'primary-alt'} label={'Primary-alt button'} />
        </div>


        <h1>el=LinkButton</h1>
        <div>
          <Link type={'link-button'} label={'link-button'} icon={'icon'} iconPosition={'left'} disabled />
        </div>


      </div>
    );
  }
}

export default App;