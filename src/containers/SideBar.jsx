import React, { Component } from 'react';
import * as CustomComponents from 'react-cl';
import { connect } from 'react-redux';
import { displayComponentActionCreator } from '../actions/index'
console.log('CustomComponents ', CustomComponents);

class SideBar extends Component {
  constructor (props) {
    super(props);

  }

  displayComponent = (CustomComponent) => {
    console.log('____component ', CustomComponent)
    this.props.displayComponent(CustomComponent);
  }

  renderList = () => {
    const namesArr = Object.keys(CustomComponents);

    console.log('CustomComponents  .......... ', CustomComponents);

    console.log('CustomComponents ', namesArr);

    return namesArr.map((CustomComponent, index) => {
        return <li key={index} onClick={() => this.displayComponent(CustomComponent)}>
          { CustomComponent }
          </li>
    });
  }

  render () {
    return(<div>
      <ul>
        { this.renderList() }
      </ul>
    </div>)
  }
}

const mapStateToProps = ({ displayComponent }) => {
  console.log('SideBar....', displayComponent );
  return {displayComponent}
};
const mapDispatchToProps = (dispatch) => {
  return ({ displayComponent: (name)=> dispatch(displayComponentActionCreator(name))})
}


//export default connect(({ displayComponent }) => displayComponent, mapDispatchToProps)(SideBar);
export default connect(mapStateToProps, mapDispatchToProps)(SideBar);

//export default SideBar;
