import React from 'react';
import styled, { css } from 'styled-components';

const IconEl = () => {
  return(<div className={'icon'}>x</div> )
}

const IconStyled = styled.div`
  width: 1em;
  height: 1em;
  background: red;
  
  ${props => (props.position === 'left') && css`
    float: left;
  `}
  
  ${props => (props.position === 'right') && css`
    float: right;
  `}
`;

export default IconStyled;
