import styled, { css } from 'styled-components'

const DefaultButton = styled.button`
  border-radius: 3px;
  padding: .687em;
  background-color: #a6a6a6;
  color: #fff;
  border: .062em solid #e0e0e0;
  cursor: pointer;
  text-decoration: none;
  vertical-align: top;
  display: inline-block;
  overflow: visible;
  text-transform: uppercase;
  text-align: center;
  font-size: 1em;
  border-radius: 25px;
  min-width: 220px;
  &:hover {
        background: #838383;
  }
  
  ${props => (props.type === 'primary') && css`
    background: #f60;
    color: white;
      &:hover {
        background: #ef4900;
      }
  `}
  
  ${props => (props.type === 'secondary') && css`
    background: #3c3c3c;
    color: white;
      &:hover {
        background: #000;
      }
  `}
  
  ${props => (props.type === 'tertiary') && css`
    background: #fff;
    border-color: #3c3c3c;
    color: #3c3c3c;
    &:hover {
        background: #fff;
        border-color: #3c3c3c;
        color: #3c3c3c;
      }
  `}
  
  ${props => (props.type === 'clearance') && css`
    background-color: #d52b1e;
      &:hover {
        background: #b6251a;
      }
  `}
  
  ${props => (props.type === 'homedelivery') && css`
    background-color: #009b3a;
      &:hover {
        background: #005900;
      }
  `}
  
  ${props => (props.type === 'clickcollect') && css`
    background-color: #003da6;
      &:hover {
        background: #001a76;
      }
  `}
  
  ${props => (props.type === 'text') && css`
    background-color: none;
    color: #3c3c3c;
      &:hover {
        background: none;
        text-decoration: underline;
        color: #3c3c3c;
      }
  `}
  
 
  ${props => (props.type === 'disabled') && css`
    background: #ededed;
    border-color: #a6a6a6;
    color: #a6a6a6;
    
    &:hover {
      background: #ededed;
      border-color: #a6a6a6;
      color: #a6a6a6;
    }
  `}
 
  ${props => (props.type === 'multiline') && css`
    background: #f60;
    color: white;
      &:hover {
        background: #ef4900;
      }
  `}
  
  ${props => (props.type === 'selected') && css`
    background: #3c3c3c;
    color: white;
      &:hover {
        background: #000;
      }
  `}
  
  
  
  
`;

export default DefaultButton;


// .btn {
//   border-color: transparent;
//   color: #fff;
// }
//
// .btn {
//   margin-right: 0;
//   font-weight: normal;
//   min-width: 13.75em;
//   padding-bottom: .687em;
//   padding-top: .687em;
//   text-align: center;
//   text-transform: uppercase;
// }