import React, { Component } from 'react';
import DefaultLink from './styles/DefaultLink';

export default class Link extends Component {
  render() {
    console.log('this  ', this.props.type);
    return(<div>
        <DefaultLink type={this.props.type}>{this.props.label}</DefaultLink>
      </div>
    );
  }
}
