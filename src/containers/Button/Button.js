import React, { Component } from 'react';
import DefaultButton from './styles/DefaultButton';
import DefaultInputButton from './styles/DefaultInputButton';

import Icon from './styles/icon';


export default class Button extends Component {
  render() {
    const {type, icon, label, multiline, mainText, subText, iconPosition, ajax, disabled} = this.props;
    console.log('this  ', this.props);
    return(<div>

        <DefaultButton disabled type={type}>
          {icon === 'icon' &&
              <Icon position={iconPosition}>x</Icon>
          }

          {ajax === 'true' &&
              <Icon position={iconPosition}>o</Icon>
          }

          {label &&
              <span>{label}</span>
          }

          {multiline &&
            <span>{mainText}</span>
          }

          {multiline==='span' &&
            <span>{subText}</span>
          }

          {multiline==='small' &&
            <small>{subText}</small>
          }

          </DefaultButton>
      </div>
    );
  }
}