import { DISPLAY_COMPONENT } from '../common/actionTypes';

export const displayComponentActionCreator = (data) => {
  console.log('...displayComponentActionCreator... ', data);
  return {
    type: DISPLAY_COMPONENT,
    payload: data
  }
}